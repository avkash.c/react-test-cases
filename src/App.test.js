import React from "react";
import { render } from "@testing-library/react";
import { shallow } from "enzyme";
import renderer from "react-test-renderer";
import App from "./App";

// test("renders learn react link", () => {
//   const { getByText } = render(<App />);
//   const linkElement = getByText(/learn react/i);
//   expect(linkElement).toBeInTheDocument();
// });

// describe("Addition", () => {
//   it("Knows that 2 and 2 makes 4", () => {
//     expect(2 + 2).toBe(5);
//   });
// });

describe("App component", () => {
  it("start with count 0", () => {
    const wrapper = shallow(<App />);
    const text = wrapper.find("p").text();
    expect(text).toEqual("Count: 0");
  });

  it("increments count by 1 when the increment button is clicked", () => {
    const wrapper = shallow(<App />);
    const incrementBtn = wrapper.find("button.increment");
    incrementBtn.simulate("click");
    const text = wrapper.find("p").text();
    expect(text).toEqual("Count: 1");
  });

  it("decrements count by 1 when the decrement button is clicked", () => {
    const wrapper = shallow(<App />);
    const decrementBtn = wrapper.find("button.decrement");
    decrementBtn.simulate("click");
    const text = wrapper.find("p").text();
    expect(text).toEqual("Count: -1");
  });
});

it("matches the snapshot", () => {
  const tree = renderer.create(<App />).toJSON();
  expect(tree).toMatchSnapshot();
});
