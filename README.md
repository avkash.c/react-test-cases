#TEST CASES

- JEST is one of the tool for testing in react
- While creating react with create-react-app, JEST is install by default.
- File App.test.js, is by default testing file and it checks that App component can render without crashing
- run command 'npm test', it will check all test created in App.test.js
- Append the given code in App.test.js
- eg.
  //...All App.test.js code here.

        describe('Addition', () => {

        it('knows that 2 and 2 make 4', () => {

            expect(2 + 2).toBe(5);

        });

        });

- describe function is use for running the group of tests together.
- 'it' and 'test' are the same function. 'it' is alias for 'test'

For Enzyme:-

- npm install enzyme enzyme-adapter-react-16
- src/setupTests.js tells Jest and Enzyme what adapter you will be making use of.

- so add followinfg to the setupTests.js

  import { configure } from "enzyme";

  import Adapter from "enzyme-adapter-react-16";

  configure({ adapter: new Adapter() });

Shallow:-

- import { shallow } from 'enzyme';
- const wrapper = shallow(<App />); //get the wrapper for <App /> component
- const btn = wrapper.find('button.increment'); //get button element with className="increment"
- btn.simulate("click"); // It will click the button

Testing with snapshots:-

- npm install react-test-renderer //its 'randerer' not 'rander'
- On the first test execution, it will create "\_\_snapshots\_\_" folder.
- In which a App.test.js.snap will contain the JSON data of the test to compare the output every time.
